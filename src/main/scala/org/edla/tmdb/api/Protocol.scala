package org.edla.tmdb.api

import spray.json.{DefaultJsonProtocol, RootJsonFormat}

object Protocol extends DefaultJsonProtocol {

  final case class AuthenticateResult(expires_at: String, request_token: String, success: Boolean)
  final case class ProductionCountry(iso_3166_1: String, name: String)
  final case class Release(iso_3166_1: String, certification: String, release_date: String)
  final case class Genre(id: Int, name: String)
  final case class ProductionCompanie(name: String, id: Int)
  final case class SpokenLanguage(iso_639_1: String, name: String)
  final case class Collection(poster_path: Option[String], id: Int, name: String, backdrop_path: Option[String])
  final case class Person(
                           id: Int,
                           birthday: Option[String],
                           name : String,
                           gender: Int,
                           popularity: Double,
                           place_of_birth: Option[String]
                         )
  final case class Movie(
                          runtime: Option[Int],
                          status: String,
                          backdrop_path: Option[String],
                          overview: Option[String],
                          title: String,
                          vote_count: Int,
                          tagline: Option[String],
                          belongs_to_collection: Option[Collection],
                          original_title: String,
                          poster_path: Option[String],
                          production_countries: List[ProductionCountry],
                          revenue: Int,
                          homepage: Option[String],
                          imdb_id: Option[String],
                          id: Int,
                          release_date: Option[String],
                          budget: Int,
                          popularity: Double,
                          genres: List[Genre],
                          production_companies: List[ProductionCompanie],
                          adult: Boolean,
                          spoken_languages: List[SpokenLanguage]
                        )
  sealed trait  Result
  final case class MovieResult(
                                original_title: String,
                                poster_path: Option[String],
                                release_date: Option[String],
                                id: Int,
                                adult: Boolean,
                                title: String,
                                popularity: Double,
                                vote_count: Int,
                                vote_average: Int,
                                backdrop_path: Option[String]
                              )extends Result
  final case class PersonResult(
                                 popularity: Double,
                                 known_for_department: Option[String],
                                 name: String,
                                 id: Int,
                                 profile_path: Option[String],
                                 adult: Boolean,
                                 gender: Int
                               )extends Result



 // case class Employer(val name: String, val address: String, val taxno: Int)
 //   extends Identifiable

  trait  Results{
    def total_results: Int
    def page: Int
    def total_pages: Int
  }
  case class MovieResults(val total_results: Int, val results: List[MovieResult], val page: Int, val total_pages:Int) extends Results
  case class PersonResults(val total_results: Int, val results: List[PersonResult], val page: Int, val total_pages:Int) extends Results

  final case class Images(
                           still_sizes: List[String],
                           poster_sizes: List[String],
                           base_url: String,
                           profile_sizes: List[String],
                           secure_base_url: String,
                           logo_sizes: List[String],
                           backdrop_sizes: List[String]
                         )
  final case class Configuration(images: Images, change_keys: List[String])
  final case class Cast(
                         cast_id: Option[Int],//Option for compatibility with PersonCredits
                         character: Option[String], //Is not always given
                         credit_id: String,
                         name: Option[String],
                         id: Int,
                         order: Option[Int],
                         profile_path: Option[String]
                       )
  final case class Crew(
                         credit_id: String,
                         departement: Option[String],
                         id: Int,
                         job: String,
                         name: Option[String],
                         profile_path: Option[String]
                       )
  final case class Credits(id: Int, cast: List[Cast], crew: List[Crew])
  final case class Releases(id: Int, countries: List[Release])
  final case class Genres(genres: List[Genre])

  final object SortCriteria extends Enumeration {
    type Letters = Value
    val Asc = Value("asc")
    val Desc = Value("desc")
  }

  final object Criteria extends Enumeration {
    type Letters = Value
    val Popularity = Value("popularity")
    val ReleaseDate = Value("release_date")
    val Revenue = Value("revenue")
    val PrimaryReleaseDate = Value("primary_release_date")
    val OriginalTitle = Value("original_title")
    val VoteAverage = Value("vote_average")
    val VoteCount = Value("VoteCount")
  }


  final case class Error(status_code: Int, status_message: String)

  val noCrew     = Crew("", None, 0, "", Some("Unknown"), None)
  val unReleased = Release("", "", "Unknown")

  implicit val authenticateResultFormat: RootJsonFormat[AuthenticateResult] = jsonFormat3(AuthenticateResult)
  implicit val productionCountriesFormat: RootJsonFormat[ProductionCountry] = jsonFormat2(ProductionCountry)
  implicit val personFormat: RootJsonFormat[Person]                         = jsonFormat6(Person)
  implicit val genreFormat: RootJsonFormat[Genre]                           = jsonFormat2(Genre)
  implicit val genres: RootJsonFormat[Genres]                                = jsonFormat1(Genres)
  implicit val productionCompanieFormat: RootJsonFormat[ProductionCompanie] = jsonFormat2(ProductionCompanie)
  implicit val spokenLanguageFormat: RootJsonFormat[SpokenLanguage]         = jsonFormat2(SpokenLanguage)
  implicit val collectionFormat: RootJsonFormat[Collection]                 = jsonFormat4(Collection)
  implicit val movieFormat: RootJsonFormat[Movie]                           = jsonFormat22(Movie)
  implicit val movieResultFormat: RootJsonFormat[MovieResult]               = jsonFormat10(MovieResult)
  implicit val personResultFormat: RootJsonFormat[PersonResult]             = jsonFormat7(PersonResult)
  implicit val movieResultsFormat: RootJsonFormat[MovieResults]             = jsonFormat4(MovieResults)
  implicit val personResultsFormat: RootJsonFormat[PersonResults]           = jsonFormat4(PersonResults)
  implicit val imagesFormat: RootJsonFormat[Images]                         = jsonFormat7(Images)
  implicit val configurationFormat: RootJsonFormat[Configuration]           = jsonFormat2(Configuration)
  implicit val castFormat: RootJsonFormat[Cast]                             = jsonFormat7(Cast)
  implicit val crewFormat: RootJsonFormat[Crew]                             = jsonFormat6(Crew)
  implicit val creditsFormat: RootJsonFormat[Credits]                       = jsonFormat3(Credits)
  implicit val release: RootJsonFormat[Release]                             = jsonFormat3(Release)
  implicit val releases: RootJsonFormat[Releases]                           = jsonFormat2(Releases)
  implicit val error: RootJsonFormat[Error]                                 = jsonFormat2(Error)
}
