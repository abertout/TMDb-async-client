package org.edla.tmdb.api

import java.nio.file.Path

import akka.stream.IOResult
import org.edla.tmdb.api.Protocol.{AuthenticateResult, Configuration, Credits, Criteria, SortCriteria, Genres, Movie, Person, Releases, Results}

import scala.concurrent.Future

trait TmdbApi {
  def getMovie(id: Long): Future[Movie]
  def getCredits(id: Long): Future[Credits]
  def getPersonCredits(id: Long): Future[Credits]
  def getReleases(id: Long): Future[Releases]
  def getGenres(): Future[Genres]
  def getPerson(id: Long): Future[Person]
  def discover(criteria: Criteria.Value, sort: SortCriteria.Value, page: Int, withCast: Option[Long]): Future[Results]
  def getConfiguration: Future[Configuration]
  def getToken: Future[AuthenticateResult]
  def searchMovie(query: String, page: Int): Future[Results]
  def searchPerson(query: String, page: Int): Future[Results]
  def downloadPoster(movie: Movie, path: Path): Option[Future[IOResult]]
  def shutdown(): Unit

}
