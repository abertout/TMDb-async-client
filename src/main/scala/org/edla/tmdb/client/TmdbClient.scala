package org.edla.tmdb.client

import scala.language.postfixOps
import java.io.{File, FileOutputStream}
import java.net.{InetSocketAddress, URLEncoder}
import scala.concurrent.{Await, Future}
import scala.concurrent.duration.{Duration, DurationInt, FiniteDuration, SECONDS}
import org.edla.tmdb.api.Protocol.{AuthenticateResult, Configuration, Credits, Criteria, Error, Genres, Movie, MovieResults, Person, PersonResults, Releases, Results, SortCriteria}
import org.edla.tmdb.api.TmdbApi
import akka.actor.{ActorRef, ActorSystem}
import akka.event.Logging
import akka.http.scaladsl.{ClientTransport, Http}
import akka.http.scaladsl.client.RequestBuilding
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport.sprayJsonUnmarshaller
import akka.http.scaladsl.model.{HttpRequest, HttpResponse}
import akka.http.scaladsl.model.Uri.apply
import akka.http.scaladsl.unmarshalling.Unmarshal
import akka.pattern.ask
import akka.stream.{ActorMaterializer, ActorMaterializerSettings, IOResult}
import akka.stream.scaladsl.{FileIO, Flow, Sink, Source}
import akka.util.Timeout

import java.util.concurrent.CountDownLatch
import akka.NotUsed
import akka.http.scaladsl.settings.{ClientConnectionSettings, ConnectionPoolSettings}

import java.nio.file.Path

object TmdbClient {
  def apply(ApiKey: String, Language: String = "en", tmdbTimeOut: FiniteDuration = 10.seconds, proxyConfig: Option[(String, Int)] = None): TmdbClient =
    new TmdbClient(ApiKey, Language, tmdbTimeOut, proxyConfig)
}

class TmdbClient(apiKey: String, language: String, tmdbTimeOut: FiniteDuration,  proxyConfig: Option[(String,Int)]) extends TmdbApi {

  private val ApiKey             = s"api_key=${apiKey}"
  private val Language           = s"language=${language}"
  private val MaxAvailableTokens = 10
  // scalastyle:off magic.number
  private val TokenRefreshPeriod = new FiniteDuration(5, SECONDS)
  // scalastyle:on magic.number
  private val TokenRefreshAmount = 10
  private val Port               = 80

  implicit val system   = ActorSystem()
  implicit val executor = system.dispatcher

  /*implicit val materializer = ActorMaterializer(
    ActorMaterializerSettings(system)
      .withInputBuffer(
        initialSize = 1,
        maxSize = 1
      )
  )*/
  private implicit val timeout = Timeout(tmdbTimeOut)
  val log                      = Logging(system, getClass)

  log.info(s"TMDb timeout value is ${tmdbTimeOut}")

  val limiterProps =
    Limiter.props(MaxAvailableTokens, TokenRefreshPeriod, TokenRefreshAmount)
  val limiter = system.actorOf(limiterProps, name = "testLimiter")


  val clientTransport = proxyConfig.map{
    case (host,port) => ClientTransport.httpsProxy(InetSocketAddress.createUnresolved(host, port))}
    .getOrElse(ClientTransport.TCP)



  /*val poolSettings = ConnectionPoolSettings(system)
    .withConnectionSettings(ClientConnectionSettings(system))
    .withTransport(clientTransport)*/

  val clientPoolSettings = ClientConnectionSettings(system)
    .withTransport(clientTransport)

  lazy val tmdbConnectionFlow: Flow[HttpRequest, HttpResponse, Future[Http.OutgoingConnection]] =
    Http().outgoingConnection("api.themoviedb.org", Port, settings = clientPoolSettings)


  val poolClientFlow =
    Http().cachedHostConnectionPool[String]("api.themoviedb.org")

  def limitGlobal[T](limiter: ActorRef): Flow[T, T, NotUsed] = {
    import akka.pattern.ask
    import akka.util.Timeout
    Flow[T].mapAsync(1)((element: T) => {
      val limiterTriggerFuture = limiter ? Limiter.WantToPass
      limiterTriggerFuture.map((_) => element)
    })
  }

  def errorHandling(): Flow[HttpResponse, HttpResponse, NotUsed] = {
    //Flow[HttpResponse].mapAsyncUnordered(4)(response => response)
    Flow[HttpResponse].map { response =>
      if (response.status.isSuccess) response
      else {
        val err = Unmarshal(response.entity).to[Error] map { e =>
          if (e.status_code == 7) {
            throw new InvalidApiKeyException(message = e.status_message, code = e.status_code)
          } else {
            throw TmdbException(message = e.status_message, code = e.status_code)
          }
        }
        //TODO is it possible to avoid Await ?
        Await.result(err, 1 seconds)
      }
    }
  }

  def tmdbRequest(request: HttpRequest): Future[HttpResponse] =
    Source
      .single(request)
      .via(limitGlobal(limiter))
      .via(tmdbConnectionFlow)
      .via(errorHandling) runWith (Sink.head)

  private lazy val baseUrl =
    Await.result(getConfiguration(), tmdbTimeOut).images.base_url
  //could not find implicit value for parameter um:
  //https://web.archive.org/web/20180816230355/http://kto.so/2016/04/10/hakk-the-planet-implementing-akka-http-marshallers/
  /*
  def manageRequest[@specialized T](request: String): Future[T] = {
    val promise = Promise[HttpResponse]()
    queue.offer((RequestBuilding.Get(request), promise))
    promise.future.flatMap { response =>
      Unmarshal(response.entity).to[T]
    }
  }
   */

  def getTimeOut: FiniteDuration = tmdbTimeOut

  def getConfiguration(): Future[Configuration] = {
    tmdbRequest(RequestBuilding.Get(s"/3/configuration?${ApiKey}")).flatMap { response =>
      Unmarshal(response.entity).to[Configuration]
    }
  }

  def getToken(): Future[AuthenticateResult] =
    tmdbRequest(RequestBuilding.Get(s"/3/authentication/token/new?${ApiKey}"))
      .flatMap { response =>
        Unmarshal(response.entity).to[AuthenticateResult]
      }

  def getMovie(id: Long): Future[Movie] = {
    tmdbRequest(RequestBuilding.Get(s"/3/movie/${id}?${ApiKey}&${Language}"))
      .flatMap { response =>
        Unmarshal(response.entity).to[Movie]
      }
  }

  def getCredits(id: Long): Future[Credits] = {
    tmdbRequest(RequestBuilding.Get(s"/3/movie/${id}/credits?${ApiKey}&${Language}"))
      .flatMap { response =>
        Unmarshal(response.entity).to[Credits]
      }
  }

  def getPersonCredits(id: Long): Future[Credits] = {
    tmdbRequest(RequestBuilding.Get(s"/3/person/$id/movie_credits?$ApiKey&$Language"))
      .flatMap{ response =>
      Unmarshal(response.entity).to[Credits]
    }
  }

  def getReleases(id: Long): Future[Releases] = {
    tmdbRequest(RequestBuilding.Get(s"/3/movie/${id}/releases?${ApiKey}"))
      .flatMap { response =>
        Unmarshal(response.entity).to[Releases]
      }
  }


  def searchMovie(query: String, page: Int): Future[MovieResults] = {
    tmdbRequest(RequestBuilding.Get(s"/3/search/movie?$ApiKey&$Language&page=$page&query=${URLEncoder.encode(query, "UTF-8")}"))
      .flatMap{ response =>
      Unmarshal(response.entity).to[MovieResults]
    }
  }

  def searchPerson(query: String, page: Int): Future[PersonResults] = {
    tmdbRequest(RequestBuilding.Get(s"/3/search/person?$ApiKey&$Language&page=$page&query=${URLEncoder.encode(query, "UTF-8")}"))
      .flatMap{ response =>
        Unmarshal(response.entity).to[PersonResults]
      }
  }




  def shutdown(): Unit = {
    Http().shutdownAllConnectionPools().onComplete { _ =>
      system.terminate
      Await.result(system.whenTerminated, Duration.Inf)
      Limiter.system.terminate()
      Await.result(Limiter.system.whenTerminated, Duration.Inf)
      ()
    }
  }

  //http://stackoverflow.com/questions/34912143/how-to-download-a-http-resource-to-a-file-with-akka-streams-and-http
  def downloadPoster(movie: Movie, path: Path): Option[Future[IOResult]] = {
    val posterPath = movie.poster_path
    val settings   = ConnectionPoolSettings(system).withMaxOpenRequests(64)
    if (posterPath.isDefined) {
      val url = s"${baseUrl}w154${posterPath.get}"
      val result: Future[HttpResponse] =
        Http()
          .singleRequest(HttpRequest(uri = url), settings = settings)
          .mapTo[HttpResponse]
      Some(result.flatMap { resp =>
        val source = resp.entity.dataBytes
        source.runWith(FileIO.toPath(path))
      })
    } else {
      None
    }
  }

  def getGenres(): Future[Genres] = {
    tmdbRequest(RequestBuilding.Get(s"/3/genre/movie/list?$ApiKey&$Language"))
      .flatMap { response =>
      Unmarshal(response.entity).to[Genres]
    }
  }

  def getPerson(id: Long): Future[Person] = {
    tmdbRequest(RequestBuilding.Get(s"/3/person/$id?$ApiKey&$Language"))
    .flatMap { response =>
      Unmarshal(response.entity).to[Person]
    }
  }


  def discover(criteria: Criteria.Value, sort: SortCriteria.Value, page: Int, withCast: Option[Long] = None): Future[MovieResults] = {

        //3/discover/movie?api_key=66f02c7706ccaffa0bc273c6c385aace&language=fr-FR&sort_by=vote_average.desc&include_adult=true&include_video=false&page=1
    TmdbClient
    tmdbRequest(RequestBuilding.Get(s"/3/discover/movie?$ApiKey&$Language&page=$page&&include_adult=true" +
          s"&sort_by=${criteria.toString}.${sort.toString}&with_cast=${withCast.getOrElse("")}")).flatMap { response =>
      Unmarshal(response.entity).to[MovieResults]
    }
  }


}

