package org.edla.tmdb

import java.io.File
import java.nio.file.{Files, Paths}

import org.edla.tmdb.api.Protocol.{Criteria, Movie, SortCriteria}

import scala.concurrent.Future
import scala.concurrent.duration.FiniteDuration

//import org.edla.tmdb.api.Protocol.Results
import org.edla.tmdb.client.{InvalidApiKeyException, TmdbClient}
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.time._
import org.scalatest.{GivenWhenThen, Matchers, PropSpec}

//import scala.concurrent.Future
import scala.concurrent.duration.DurationInt
import scala.util.{Failure, Success}

import scala.concurrent.ExecutionContext.Implicits.global

class TmdbAsyncClientTest extends PropSpec with Matchers with ScalaFutures with GivenWhenThen {

  val apiKey: String = sys.env("apiKey")
  val proxyHost: String = sys.env("proxyHost")
  val proxyPort: Int = sys.env("proxyPort").toInt

  val tmdbClient = TmdbClient(apiKey)
  val tmdbClientWithProxy = TmdbClient(apiKey, proxyConfig = Some((proxyHost,proxyPort)))

  implicit val timeout: FiniteDuration = 10.seconds
  implicit val defaultPatience: PatienceConfig = PatienceConfig(timeout = Span(5, Seconds), interval = Span(1, Seconds))

  property("Authentication with valid API key should be successfull") {
    whenReady(tmdbClient.getToken) { authenticateResult =>
      authenticateResult.success should be(true)
    }
  }

  property("Authentication with valid API key through proxy should be successfull") {
    whenReady(tmdbClientWithProxy.getToken) { authenticateResult =>
      authenticateResult.success should be(true)
    }
  }

  property("Search movie by name should return results through proxy") {
    whenReady(tmdbClientWithProxy.searchMovie("shark", 1)) { results =>
      results.total_results should be > 5
    }
  }


  property("Authentication with invalid API key should throw InvalidApiKeyException") {
    TmdbClient("00000000000000000000000000000000").getToken.failed.futureValue shouldBe an[InvalidApiKeyException]
  }

  property("Search movie by name should return results") {
    whenReady(tmdbClient.searchMovie("shark", 1)) { results =>
      results.total_results should be > 5
    }
  }

  property("Get title from movie.id should be correct") {
    val path = Paths.get(s"${System.getProperty("java.io.tmpdir")}${File.separator}poster.jpg")
    whenReady(tmdbClient.getMovie(680)) { movie =>
      movie.title should be("Pulp Fiction")
      Then("the poster should be downloaded successfully")
      val poster = tmdbClient.downloadPoster(movie, path)
      if (poster.isDefined) {
        whenReady(poster.get) {
          _.wasSuccessful should equal(true)
        }
      }
      And("the poster should be OK")
      Files.size(path) should (be(17695) or be(17709) or be(13166))
    }
  }

  property("Do not flood remote server when download posters") {
    var count = 0
    for (pageNum <- List(1, 2)) {
      tmdbClient.searchMovie("batman", pageNum).map { movies =>
        for (result <- movies.results) {
          val movieF = tmdbClient.getMovie(result.id)
          movieF.onComplete {
            case Success(movie) =>
              count = count + 1
              val path = Paths.get(s"${System.getProperty("java.io.tmpdir")}${File.separator}${movie.id}.jpg")
              //println(movie.id + ":" + path)
              val poster = tmdbClient.downloadPoster(movie, path)
              if (poster.isDefined) {
                whenReady(poster.get) {
                  _.wasSuccessful should equal(true)
                }
              }
            case Failure(_) =>
          }
        }
      }
    }
    Thread.sleep(30000)
    count shouldBe 40
  }

  property("Get director from movie.id should be correct") {
    whenReady(tmdbClient.getCredits(680)) { credits =>
      credits.crew.find(crew => crew.job == "Director").get.name should be("Quentin Tarantino")
    }
  }

  property("Get localized release date from movie.id should be correct") {
    whenReady(tmdbClient.getReleases(680)) { releases =>
      releases.countries.find(country => country.iso_3166_1 == "US").get.release_date should be("1994-09-23")
    }
  }

  property("Respect server rating (test 1)") {
    var count = 85
    for (_ <- 1 to 85) {
      val test: Future[Movie] = tmdbClient.getMovie(680)
      test onComplete {
        case Success(_) =>
          count = count - 1
        case Failure(_) =>
      }
    }
    Thread.sleep(30000)
    count shouldBe 0
  }

  property("Respect server rating (test 2)") {

    whenReady(tmdbClient.searchMovie("life", 1)) { movies =>
      for (m <- movies.results) {
        val movie = tmdbClient.getMovie(m.id)
        movie.onComplete {
          case Failure(e) => fail(e)
          case Success(_) =>
            val credits = tmdbClient.getCredits(m.id)
            credits.onComplete {
              case Failure(e) => fail(e)
              case Success(_) =>
            }
            val releases = tmdbClient.getReleases(m.id)
            releases.onComplete {
              case Failure(e) => fail(e)
              case Success(_) =>
            }
        }
      }
    }
    whenReady(tmdbClient.searchMovie("Take me", 1)) { movies =>
      for (m <- movies.results) {
        val movie = tmdbClient.getMovie(m.id)
        movie.onComplete {
          case Failure(e) => fail(e)
          case Success(_) =>
            val credits = tmdbClient.getCredits(m.id)
            credits.onComplete {
              case Failure(e) => fail(e)
              case Success(_) =>
            }
            val releases = tmdbClient.getReleases(m.id)
            releases.onComplete {
              case Failure(e) => fail(e)
              case Success(_) =>
            }
        }
      }
    }
    Thread.sleep(2 * timeout.toMillis)
  }

  property("TMDb should shutdown gracefully") {
    tmdbClient.shutdown()
  }

  property("Get all genres should be correct") {
    whenReady(tmdbClient.getGenres()) { allgenres =>
      allgenres.genres.size should be > 0
    }

  }


  property("Get person details from person.id should be correct") {
    whenReady(tmdbClient.getPerson(56024)) { details =>
      details.name should be("Jean Dujardin")
    }
  }


  property("Discover by criteria should return results") {
    whenReady(tmdbClient.discover(Criteria.Popularity, SortCriteria.Desc, 1)) { results =>
      results.total_results should be > 15
    }
  }

  property("Discover with existing cast should return results") {
    whenReady(tmdbClient.discover(Criteria.Popularity, SortCriteria.Desc, 1,withCast = Some(56024))){ results =>
      results.total_results should be > 2
    }
  }

  property("Get person credits should be correct") {
    whenReady(tmdbClient.getPersonCredits(138)){ results =>
      results.crew.length should be > 2
    }
  }
}
